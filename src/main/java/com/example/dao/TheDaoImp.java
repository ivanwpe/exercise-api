package com.example.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.*;

@Transactional

@Repository

public class TheDaoImp implements TheDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Employee> getAllEmployee() {
		String sql = "select * from employee";
		List<Employee> employee = jdbcTemplate.query(sql, new EmployeeMapper());
		return employee;
	}

	@Override
	public List<EmployeeRole> getAllRole() {
		String sql = "select * from employeerole";
		List<EmployeeRole> employeeRole = jdbcTemplate.query(sql, new EmployeeRoleMapper());
		return employeeRole;
	}

	@Override
	public Employee getEmployeeById(int id) {
		String sql2 = "select * from employee where employeeid = ?";
		Employee employee2 = jdbcTemplate.queryForObject(sql2, new Object[] { id }, 
				new EmployeeMapper());
		return employee2;
	}

	@Override
	public void addEmployee(Employee employee) {
		String sql = "INSERT INTO employee (employeeroleid, employeename, phone, address) "
				+ "VALUES (?,?,?,?) ";
		jdbcTemplate.update(sql, employee.getRoleId() , employee.getName(), 
				employee.getPhone(), employee.getAddress());
	}

	@Override
	public void updateEmployee(Employee employee, int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteEmployeeById(int id) {
		String sql = "delete from employee where employeeid=?";
		jdbcTemplate.update(sql, id);
	}

	@Override
	public EmployeeRole getEmployeeRoleById(int roleId) {
		String sql = "select * from employeerole where employeeroleid = ?";
		EmployeeRole employeeRole2 = jdbcTemplate.queryForObject(sql, new Object[] { roleId }, new EmployeeRoleMapper());
		return employeeRole2;
	}

	@Override
	public void addEmployeeRole(EmployeeRole employeeRole) {
		String sql = "INSERT INTO employeerole (employeerolename) select (?) ";
		jdbcTemplate.update(sql, employeeRole.getName());
	}

	@Override
	public void updateEmployeeRole(EmployeeRole employeeRole, int roleId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteEmployeeRoleById(int roleId) {
		String sql = "delete from employeerole where employeeroleid=?";
		jdbcTemplate.update(sql, roleId);
	}

	@Override
	public int lastestInput() {
		String sql2 = "SELECT currval(pg_get_serial_sequence('employee','employeeid'))";
		int id = jdbcTemplate.queryForObject(sql2, Integer.class);
		return id;
	}
	@Override
	public int lastestInputRole() {
		String sql2 = "SELECT currval(pg_get_serial_sequence('employeerole','employeeroleid'))";
		int roleId = jdbcTemplate.queryForObject(sql2, Integer.class);
		return roleId;
	}

}
