package com.example.dao;

import java.util.List;

import com.example.model.Employee;
import com.example.model.EmployeeRole;

public interface TheDao {

	List<Employee> getAllEmployee();

	List<EmployeeRole> getAllRole();

	Employee getEmployeeById(int id);

	void addEmployee(Employee employee);

	void updateEmployee(Employee employee, int id);

	void deleteEmployeeById(int id);

	EmployeeRole getEmployeeRoleById(int id);

	void addEmployeeRole(EmployeeRole employeeRole);

	void updateEmployeeRole(EmployeeRole employeeRole, int id);

	void deleteEmployeeRoleById(int id);
	
	int lastestInput();
	
	int lastestInputRole();
}
