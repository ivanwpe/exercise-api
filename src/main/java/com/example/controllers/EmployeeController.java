package com.example.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.dao.TheDao;
import com.example.model.*;

@Controller

@RequestMapping("employee")

public class EmployeeController {

	@Autowired
	TheDao theDao;

	@GetMapping("/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") int id) {
		Employee employee = theDao.getEmployeeById(id);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}

	@GetMapping("/")
	public ResponseEntity<List<Employee>> getAllEmployee() {
		List<Employee> employee = theDao.getAllEmployee();
		return new ResponseEntity<List<Employee>>(employee, HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee) {
		theDao.addEmployee(employee);
		Employee employee2 = theDao.getEmployeeById(theDao.lastestInput());
		return new ResponseEntity<Employee>(employee2, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Employee> deleteEmployee(@PathVariable("id") int id) {
		Employee employee2 = theDao.getEmployeeById(id);
		theDao.deleteEmployeeById(id);
		return new ResponseEntity<Employee>(employee2, HttpStatus.OK);
	}
}
