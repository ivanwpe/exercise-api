package com.example.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.dao.TheDao;
import com.example.model.*;

@Controller

@RequestMapping("employeerole")

public class EmployeeRoleController {

	@Autowired
	TheDao theDao;

	@GetMapping("/{id}")
	public ResponseEntity<EmployeeRole> getEmployeeRoleById(@PathVariable("id") int roleId) {
		EmployeeRole employeeRole = theDao.getEmployeeRoleById(roleId);
		return new ResponseEntity<EmployeeRole>(employeeRole, HttpStatus.OK);
	}

	@GetMapping("/")
	public ResponseEntity<List<EmployeeRole>> getAllRole() {
		List<EmployeeRole> listEmpRole = theDao.getAllRole();
		return new ResponseEntity<List<EmployeeRole>>(listEmpRole, HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<EmployeeRole> addEmployeeRole(@RequestBody EmployeeRole employeeRole) {
		theDao.addEmployeeRole(employeeRole);
		EmployeeRole employeeRole2 = theDao.getEmployeeRoleById(theDao.lastestInputRole());
		return new ResponseEntity<EmployeeRole>(employeeRole2, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<EmployeeRole> deleteEmployeeRole(@PathVariable("id") int roleId) {
		EmployeeRole employeeRole2 = theDao.getEmployeeRoleById(roleId);
		theDao.deleteEmployeeRoleById(roleId);
		return new ResponseEntity<EmployeeRole>(employeeRole2, HttpStatus.OK);
	}
}
