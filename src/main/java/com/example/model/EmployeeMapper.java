package com.example.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EmployeeMapper implements RowMapper<Employee> {

	@Override
	public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
		Employee employee = new Employee();
		employee.setId(rs.getInt("employeeid"));
		employee.setRoleId(rs.getInt("employeeroleid"));
		employee.setName(rs.getString("employeename"));
		employee.setPhone(rs.getString("phone"));
		employee.setAddress(rs.getString("address"));
		return employee;
	}

}
