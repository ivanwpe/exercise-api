package com.example.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EmployeeRoleMapper implements RowMapper<EmployeeRole> {

	@Override
	public EmployeeRole mapRow(ResultSet rs, int rowNum) throws SQLException {
		EmployeeRole employeeRole = new EmployeeRole();
		employeeRole.setRoleId(rs.getInt("employeeroleid"));
		employeeRole.setName(rs.getString("employeerolename"));
		return employeeRole;
	}

}
